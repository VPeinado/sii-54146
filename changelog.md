# CHANGELOG
Todos los cambios notables de este proyecto se documentarán en este archivo.

## [1.5] - 2020/12/10
### AÑADIDO
- Clase socket (.h y .cpp)
- Comunicacion entre cliente y servidor mediante sockets

### CAMBIOS
- Ahora hay que introducir un nombre de cliente antes de jugar
- La tuberia del logger ahora se llama logger para mas claridad

### ELIMINADO
- Comunicacion entre cliente y servidor mediante tuberias

## [1.4] - 2020/11/26
### AÑADIDO
- Programa cliente
- Programa servidor
- Clase MundoServidor y clase MundoCliente
- Tratamiento de algunas señales

### CAMBIOS
- Las funciones del Mundo se han dividido entre MundoCliente y en MundoServidor
- El CMakeLists se ha actualizado para crear los ejecutables de los nuevos Mundos

### ELIMINADO
- Clases anteriores Mundo y tenis

## [1.3] - 2020/11/12
### AÑADIDO
- Programa logger
- Programa bot
- Estructura en Puntos.h para el logger
- Clase en DatosMemCompartida.h para el bot

### CAMBIOS
- El juego se termina al llegar a 3 puntos
- Cambios en mundo.cpp para la implementacion del logger y el bot
- Formateo del changelog a changelog.md


## [1.2] - 2020/10/22
### AÑADIDO
- Archivo README de instrucciones del juego

### CAMBIOS
- Funcionalidad de movimiento de las raquetas
- La pelota disminuye su tamaño segun avanza el tiempo y se reinicia al marcar


## [1.1] - 2020/10/08
### AÑADIDO
- Archivo changelog de cambios notables en las versiones

### CAMBIOS
- Nombre del autor añadido en todos los archivos
