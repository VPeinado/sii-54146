//Autor: Víctor Peinado López

// MundoServidor.cpp: implementation of the CMundoServidor class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "Puntos.h"

#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <iostream>
#include <pthread.h>
#include <signal.h>


static void CerrarServidor(int n)
{
	exit(1);
}

void* hilo_comandos(void* d)
{
	CMundoServidor* p=(CMundoServidor*) d;
	p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador()
{
	while(1){
		usleep(10);
		char cad[100];
		if (s_comunicacion.Receive(cad, sizeof(cad)) == SOCKET_ERROR)
			this->CMundoServidor::~CMundoServidor();
		unsigned char key;
		sscanf(cad, "%c", &key);
		if(key=='s')jugador1.velocidad.y=-4;
		if(key=='w')jugador1.velocidad.y=4;
		if(key=='l')jugador2.velocidad.y=-4;
		if(key=='o')jugador2.velocidad.y=4;
	}
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	close(fd);
	unlink("logger");
	
	s_conexion.Close();
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
//VARIABLES LOCALES Y FUNCIONES DE MOVIMINETO
        Puntos puntos;
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	
//GESTION DE REBOTES
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	
	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);

	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		esfera.radio = 0.5f;
		puntos2++;
		puntos.jugador1 = puntos1;
		puntos.jugador2 = puntos2;
		puntos.ganador = 2;
		write(fd, &puntos, sizeof(puntos));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		esfera.radio = 0.5f;
		puntos1++;
		puntos.jugador1 = puntos1;
		puntos.jugador2 = puntos2;
		puntos.ganador = 1;
		write(fd, &puntos, sizeof(puntos));
	}
//VALORES PARA EL LOGGER PARA MENSAJE DE FINAL DEL JUEGO
	if (puntos1 >= 3){
		puntos.ganador = 3;
		write(fd, &puntos, sizeof(puntos));
	}
	else if (puntos2 >= 3){
		puntos.ganador = 4;
		write(fd, &puntos, sizeof(puntos));
	}
//IMPRESION DE LOS DATOS A ENVIAR
	char cad[200];
	sprintf(cad, "%f %f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x, esfera.centro.y, esfera.radio, jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2, jugador2.x1, jugador2.y1, jugador2.x2, jugador2.y2, puntos1, puntos2);
	s_comunicacion.Send(cad, sizeof(cad));

//FINALIZACION DEL SERVIDOR AL LLEGAR A 3 PUNTOS
	if (puntos1 >= 3 || puntos2 >= 3)
		this->CMundoServidor::~CMundoServidor();
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	//EL TECLADO ES GESTIONADO POR EL CLIENTE
}

void CMundoServidor::Init()
{
//SOCKET
	char ip[] = "127.0.0.1";	//IP LOCAL
	if(s_conexion.InitServer(ip, 8000) == -1)
		printf("Error abriendo el servidor\n");
	s_comunicacion = s_conexion.Accept();
	char nombre[50];
	s_comunicacion.Receive(nombre, sizeof(nombre));
	printf("%s se ha conectado al servidor\n", nombre);
	
//APERTURA DE FIFO DEL LOGGER
	if ((fd = open("logger", O_WRONLY)) < 0) {
	  perror("Error abriendo el FIFO del logger en escritura");
	  return;
	}

//CAPTURA DE SEÑALES
	struct sigaction accion;
	accion.sa_handler = &CerrarServidor;
	if(sigaction(SIGINT, &accion, NULL) < 0){
		printf("Error por señal SIGINT PID(%d)\n", getpid());
		return;
	}
	accion.sa_handler = &CerrarServidor;
	if(sigaction(SIGTERM, &accion, NULL) < 0){
		printf("Error por señal SIGTERM PID(%d)\n", getpid());
		return;
	}
	accion.sa_handler = &CerrarServidor;
	if(sigaction(SIGPIPE, &accion, NULL) < 0){
		printf("Error por señal SIGPIPE PID(%d)\n", getpid());
		return;
	}
	accion.sa_handler = &CerrarServidor;
	if(sigaction(SIGUSR2, &accion, NULL) < 0){
		printf("Error por señal SIGUSR2 PID(%d)\n", getpid());
	}

//CREACION DEL THREAD
	pthread_attr_init(&atrib);
	pthread_attr_setdetachstate(&atrib, PTHREAD_CREATE_JOINABLE);
	pthread_create(&thid1, &atrib, hilo_comandos, this);	
	
//INICIO PARA EL DIBUJO
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}

