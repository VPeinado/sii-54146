//Autor: Víctor Peinado López

// MundoCliente.cpp: implementation of the CMundoCliente class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "Puntos.h"

#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <iostream>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();	//Funcion de inicio del cliente
}

CMundoCliente::~CMundoCliente()
{
	pdatosmem->accion = 2;		//Accion = 2 para cerrar bot
	munmap(pdatosmem, sizeof(datosmem));
	unlink("datosmem");
	
	s_comunicacion.Close();
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
//ESCANEO DE LOS DATOS A RECIBIR
	char cad[200];
	s_comunicacion.Receive(cad, sizeof(cad));	//RECEPCION DE DATOS DEL SOCKET
	sscanf(cad, "%f %f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x, &esfera.centro.y, &esfera.radio, &jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2, &jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1, &puntos2);
//ASIGNACION DE DATOS A LA MEMORIA COMPARTIDA
	pdatosmem->esfera = esfera;
	pdatosmem->raqueta1 = jugador1;
	
	if (pdatosmem->accion == -1){OnKeyboardDown('s', 0, 0);}
	else if (pdatosmem->accion == 0){}
	else if (pdatosmem->accion == 1){OnKeyboardDown('w', 0, 0);}
//FINALIZACION DEL CLIENTE AL LLEGAR A 3 PUNTOS
	if (puntos1 >= 3 || puntos2 >= 3){
		this->CMundoCliente::~CMundoCliente();
	}
}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
//CONVERSION: unsigned char EN char*
	char keychar[] = "0";
	switch(key)
	{
	case 's':
		sprintf(keychar, "s");
		break;
	case 'w':
		sprintf(keychar, "w");
		break;
	case 'l':
		sprintf(keychar, "l");
		break;
	case 'o':
		sprintf(keychar, "o");
		break;
	}
//ENVIO DE DATOS DEL SOCKET
	s_comunicacion.Send(keychar, sizeof(keychar));
}

void CMundoCliente::Init()
{
//CONEXION DEL SOCKET
	char ip[] = "127.0.0.1";	//IP LOCAL
	char nombre[50];
	printf("Introduzca su nombre de usuario:\n");
	scanf("%s",nombre);
	s_comunicacion.Connect(ip, 8000);
	s_comunicacion.Send(nombre, sizeof(nombre));

//DATOS PARA EL BOT
	int fd_mmap;
	fd_mmap = open("datosmem", O_CREAT|O_TRUNC|O_RDWR, 0666);
	if (fd_mmap < 0){
	  perror("Error creando la proyeccion de los datos compartidos");
	  exit(1);
	}
	datosmem.esfera = esfera;
	datosmem.raqueta1 = jugador1;
	datosmem.accion = 0;
	write(fd_mmap, &datosmem, sizeof(datosmem));
	pdatosmem = static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(datosmem), PROT_READ|PROT_WRITE, MAP_SHARED, fd_mmap, 0));
	close(fd_mmap);
	if (pdatosmem == MAP_FAILED){
	  perror("Error proyectado los datos compartidos");
	  return;
        }

//INICIO PARA EL DIBUJO
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}

