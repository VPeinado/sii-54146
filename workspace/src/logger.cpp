#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include "Puntos.h"

int main ()
{
int fd;
Puntos puntos;

if (mkfifo("logger", 0666) < 0){
  perror("Error creando el FIFO");
  return 1;
}

if ((fd = open("logger", O_RDONLY)) < 0){
  perror("Error abriendo el FIFO");
  return 1;
}

while(read(fd, &puntos, sizeof(puntos))==sizeof(puntos)){

  if (puntos.ganador == 1){
    printf("J1 ha marcado punto | J1 (%d - %d) J2 |\n", puntos.jugador1, puntos.jugador2);
  }
  else if (puntos.ganador == 2){
    printf("J2 ha marcado punto | J1 (%d - %d) J2 |\n", puntos. jugador1, puntos.jugador2);
  }
  else if (puntos.ganador == 3){
    printf("J1 ha ganado %d a %d. ¡Enhorabuena J1!\n", puntos.jugador1, puntos.jugador2);
  }
  else if (puntos.ganador == 4){
    printf("J2 ha ganado %d a %d. ¡Enhorabuena J2!\n", puntos.jugador2, puntos.jugador1);
  }
}
if (close(fd) < 0)
  perror("Error cerrando el FIFO");

  return(0);
}
