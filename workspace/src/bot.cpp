#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>

int main(void){
  int fd;
  DatosMemCompartida *pdatosmem;
  fd = open("datosmem", O_RDWR);
  if (fd < 0){
    perror("Error abriendo el fichero");
    return 1;
  }
  
  pdatosmem = static_cast<DatosMemCompartida*>(mmap(0,sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0));
  
  if (pdatosmem == MAP_FAILED){
    perror("Error proyectando el archivo");
    close(fd);
    return 1;
  }
  close(fd);
  
  while(pdatosmem != NULL){
    float centroraqueta = (pdatosmem->raqueta1.y1 + pdatosmem->raqueta1.y2)/2;
    
    if(pdatosmem->accion == 2)
      pdatosmem = NULL;
    else if(pdatosmem->esfera.centro.y < centroraqueta)
      pdatosmem->accion = -1;
    else if(pdatosmem->esfera.centro.y == centroraqueta)
      pdatosmem->accion = 0;
    else if(pdatosmem->esfera.centro.y > centroraqueta)
      pdatosmem->accion = 1;
    usleep(25000);
  }
  return 1;
}
